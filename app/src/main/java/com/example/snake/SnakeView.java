package com.example.snake;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;

import java.util.ArrayList;
import java.util.Random;

public class SnakeView extends GridView{

    private final String LOG_TAG = MainActivity.class.getName();

    public Menu menu;

    private final int TILE_EMPTY = -1;
    private final int TILE_WALL = 0;
    private final int TILE_SNAKE_HEAD = 1;
    private final int TILE_SNAKE_PART = 2;
    private final int TILE_FOOD = 3;

    public ArrayList<Coordinate> mSnakeTrail = new ArrayList<Coordinate>();
    private Coordinate mSnakeHeadCoordinate;
    private Coordinate mFoodCoordinate;

    public int mDirection = NORTH;
    private int mNextDirection = NORTH;
    public static final int NORTH = 1;
    public static final int SOUTH = 2;
    public static final int EAST = 3;
    public static final int WEST = 4;

    public int mMode = READY;
    public static final int PAUSE = 0;
    public static final int READY = 1;
    public static final int RUNNING = 2;
    public static final int LOSE = 3;

    public int TIMER = 600;
    public int score = 0;


    public SnakeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initSnakeView(context);
    }

    public SnakeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSnakeView(context);
    }

    private void initSnakeView(Context context){
        Log.i(LOG_TAG, "Initializing...");
        setFocusable(true);
        Resources r = this.getContext().getResources();
        resetTileList(TILE_FOOD + 1);

        loadTile(TILE_WALL, r.getDrawable(R.drawable.wall));
        loadTile(TILE_SNAKE_HEAD, r.getDrawable(R.drawable.snake_head));
        loadTile(TILE_SNAKE_PART, r.getDrawable(R.drawable.snake_body));
        loadTile(TILE_FOOD, r.getDrawable(R.drawable.food));

        this.updateTiles();
    }

    @Override
    protected void generateFood() {
        Log.i(LOG_TAG, "Create random food...");
        Random x = new Random();
        int xValue = x.nextInt(mNbTileX);
        Random y = new Random();
        int yValue = y.nextInt(mNbTileY);

        int tileValue = getTileValue(xValue, yValue);
        Log.i(LOG_TAG, "Tile value : "+tileValue);

        if (tileValue < 3 && tileValue >= 0){
            generateFood();
        }else{
            mFoodCoordinate = new Coordinate(xValue, yValue);
        }
    }

    @Override
    protected void generateSnake(){

        Log.i(LOG_TAG, "Create random snake head...");
        Random x = new Random();
        int xValue = x.nextInt(mNbTileX);
        Random y = new Random();
        int yValue = y.nextInt(mNbTileY);

        int tileValue = getTileValue(xValue, yValue);
        Log.i(LOG_TAG, "Tile value : "+tileValue);

        if (tileValue < 3 && tileValue >= 0){
            generateSnake();
        }else{
            mSnakeHeadCoordinate = new Coordinate(xValue, yValue);
            mSnakeTrail.add(mSnakeHeadCoordinate);
            mNextDirection = NORTH;
        }

    }

    @Override
    protected void updateTiles() {

        updateWalls();
        updateSnake();
        updateFood();
        invalidate();

    }

    @Override
    protected void updateWalls() {
        Log.i(LOG_TAG, "Updating walls...");

        for (int x=0; x < mNbTileX; x++){
            setTile(TILE_WALL, x ,0);
            setTile(TILE_WALL, x ,mNbTileY - 1);
        }

        for (int y=0; y < mNbTileY; y++){
            setTile(TILE_WALL, 0 ,y);
            setTile(TILE_WALL, mNbTileX - 1 ,y);
        }
    }

    @Override
    protected void updateFood() {
        Log.i(LOG_TAG, "Updating food...");
        Log.i(LOG_TAG, "Food position : "+mFoodCoordinate);
        if (mFoodCoordinate != null){
            setTile(TILE_FOOD, mFoodCoordinate.x , mFoodCoordinate.y);
        }

    }

    private void updateSnake() {
        boolean growSnake = false;

        if(mSnakeTrail.size() > 0){

            // Grab the snake by the head
            Coordinate head = mSnakeTrail.get(0);
            Coordinate newHead = new Coordinate(head.x, head.y);
            mDirection = mNextDirection;
            switch (mDirection) {
                case EAST: {
                    newHead = new Coordinate(head.x + 1, head.y);
                    break;
                }
                case WEST: {
                    newHead = new Coordinate(head.x - 1, head.y);
                    break;
                }
                case NORTH: {
                    newHead = new Coordinate(head.x, head.y - 1);
                    break;
                }
                case SOUTH: {
                    newHead = new Coordinate(head.x, head.y + 1);
                    break;
                }
            }

            // Collision detection
            int tileValue = getTileValue(newHead.x, newHead.y);
            if (tileValue >= 0 && tileValue < 3 ){
                setMode(LOSE);
            }

            if (tileValue == 3 ){
                score++;
                Log.i(LOG_TAG, "collision food");
                generateFood();
                growSnake = true;
                setScore();
                score++;

                TIMER = (int) (TIMER / (1 + (score / 50)));
            }

            // push a new head onto the ArrayList and pull off the tail
            mSnakeTrail.add(0, newHead);
            // except if we want the snake to grow
            if (!growSnake) {
                setTile(TILE_EMPTY, mSnakeTrail.get(mSnakeTrail.size() - 1).x, mSnakeTrail.get(mSnakeTrail.size() - 1).y);
                mSnakeTrail.remove(mSnakeTrail.size() - 1);
            }

            int index = 0;
            for (Coordinate c : mSnakeTrail) {
                if (index == 0) {
                    setTile(TILE_SNAKE_HEAD, c.x, c.y);
                } else {
                    setTile(TILE_SNAKE_PART, c.x, c.y);
                }
                index++;
            }

        }

    }

    public void moveSnake(int direction) {
        if (direction == Snake.MOVE_UP) {

            if (mDirection != SOUTH) {
                mNextDirection = NORTH;
            }
            return;
        }
        if (direction == Snake.MOVE_DOWN) {
            if (mDirection != NORTH) {
                mNextDirection = SOUTH;
            }
            return;
        }
        if (direction == Snake.MOVE_LEFT) {
            if (mDirection != EAST) {
                mNextDirection = WEST;
            }
            return;
        }
        if (direction == Snake.MOVE_RIGHT) {
            if (mDirection != WEST) {
                mNextDirection = EAST;
            }
            return;
        }
    }


    public void initNewGame() {
        mSnakeTrail.clear();
        mFoodCoordinate = null;
    }

    public void setMode(int newMode) {
        int oldMode = mMode;
        mMode = newMode;
        if (newMode == RUNNING && oldMode != RUNNING) {
            // hide the game instructions
            this.menu.getItem(0).setIcon(R.drawable.ic_stop_solid);
        }

        if (newMode == PAUSE) {

        }
        if (newMode == READY) {

        }
        if (newMode == LOSE) {
            this.initNewGame();
            this.clearTiles();
            this.menu.getItem(0).setIcon(R.drawable.ic_snake);
        }

    }

    public void setScore(){
        Log.i(LOG_TAG, "Set score...");
    }

}
