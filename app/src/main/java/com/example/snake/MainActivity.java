package com.example.snake;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;

public class MainActivity extends AppCompatActivity {
    private final String LOG_TAG = MainActivity.class.getName();

    private Menu mainMenu;
    private SnakeView snakeView;

    private RedrawHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, getString(R.string.debug_creation_msg));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.snakeView = findViewById(R.id.snakeView);
        this.snakeView.setOnTouchListener(handleTouch);
        this.mHandler = new RedrawHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        this.mainMenu = menu;
        this.snakeView.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.start:
                if (this.snakeView.mMode == 2){
                    this.snakeView.setMode(this.snakeView.LOSE);
                    stopGame();
                }else{
                    this.snakeView.setMode(this.snakeView.RUNNING);
                    newGame();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void newGame(){

        Log.i(LOG_TAG, "Start game");

        this.snakeView.initNewGame();
        this.snakeView.generateFood();
        this.snakeView.generateSnake();
        this.snakeView.updateTiles();

        this.mHandler.setInterval(this.snakeView.TIMER);
        this.mHandler.request();

    }

    public void stopGame(){

        Log.i(LOG_TAG, "Stop game");
        this.snakeView.initNewGame();
        this.snakeView.clearTiles();


    }

    public void update() {
        Log.i(LOG_TAG, "update");

        if (this.snakeView.mMode == 2){
            this.mHandler.setInterval(this.snakeView.TIMER);
            this.mHandler.request();
            this.snakeView.updateTiles();
        }

    }

    public void changeDirection(int x, int y){

        if(this.snakeView.mDirection == this.snakeView.SOUTH || this.snakeView.mDirection == this.snakeView.NORTH ){
            if(x >= this.snakeView.getTileXPos(this.snakeView.mSnakeTrail.get(0).x) ){
                this.snakeView.moveSnake(this.snakeView.EAST);
            }else{
                this.snakeView.moveSnake(this.snakeView.WEST);
            }
        }

        if (this.snakeView.mDirection == this.snakeView.WEST || this.snakeView.mDirection == this.snakeView.EAST ){
            if(y >= this.snakeView.getTileYPos(this.snakeView.mSnakeTrail.get(0).y) ){
                this.snakeView.moveSnake(this.snakeView.SOUTH);
            }else{
                this.snakeView.moveSnake(this.snakeView.NORTH);
            }
        }

    }

    private View.OnTouchListener handleTouch = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int x = (int) event.getX();
            int y = (int) event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_UP:
                    Log.i("TAG", "touched down " + x + " " + y);
                    changeDirection(x, y);
                    break;
            }

            return true;
        }
    };
}
